#!/usr/bin/python

import threading
import time
import os
import subprocess
import sys
import re

ignore_file = "./yumignore";
MAX_LEVEL = 3;

ignore_file_names = [];
ignore_hasdone_names = [];

def fill_ignore_file_names():
    fp = open(ignore_file, "r");
    filenames = fp.readlines();
    fp.close();
    for filename in filenames:
        ignore_file_names.append(filename.strip());

def fill_ignore_hasdone_names():
    handle = subprocess.Popen('ls', stdout=subprocess.PIPE, shell=True);
    info =  handle.stdout.read().strip();
    handle.stdout.close();    

    index = 0;
    count = len(lib_list);

    filenames = info.split();
    for filename in filenames:
        if(filename.find(".x86_64.rpm") >=0 ):
            ignore_hasdone_names.append(filename);  

def ignore_file_depend_libs(lib_list):
    if(os.path.isfile(ignore_file) == False):
        return -1;
    if(len(ignore_file_names) <= 0):
        return -2;

    index = 0;
    for filename in ignore_file_names:
        filename = filename.strip();
        print "Ignore file[%d]:%s" % (index, filename);
        if(lib_list.count(filename) > 0):
            index += 1;
            del lib_list[lib_list.index(filename)];

def ignore_hasdone_libs(lib_list):
    if(len(ignore_hasdone_names) <= 0):
        return -2;
    for libname in lib_list:
	for filename in ignore_hasdone_names:
            if(filename.find(libname) >= 0):
                print "Ignore hasdone[%d]:%s" % (index, libname);
                index = index + 1;
                del lib_list[lib_list.index(libname)];
                break;
    
def fill_depend_libs(name, lib_list, level):
    handle = subprocess.Popen('yum deplist ' + name, stdout=subprocess.PIPE, shell=True);
    info =  handle.stdout.read().strip();
    handle.stdout.close();

    deps_count = 0;
    info_array = info.split("\n");
    for i in range(0, len(info_array), 1):
        if(info_array[i].find("provider") >= 0):
            sub_infos = info_array[i].split();
            lib_name = sub_infos[1][0:sub_infos[1].find(".")];
            lib_name = lib_name.strip();
           
            path = ""
            for i in range(0, level):
                path += "  "
            path += str(level) + "|----" + lib_name;

 
            need_ignore = False;
            for filename in ignore_file_names:
                if(lib_name == filename):
                    need_ignore = True;
                    break;
            if(need_ignore):
                continue;
            for filename in ignore_hasdone_names:
                if(filename.find(lib_name) >=0 ):
                    need_ignore = True;
                    break;
            if(need_ignore):
                continue;

            if(lib_list.count(lib_name) == 0):
                deps_count += 1;
                lib_list.append(lib_name);
                print path;
                if(level < MAX_LEVEL):
                    fill_depend_libs(lib_name, lib_list, level + 1);
    return deps_count;

def download_lib(name):
    os.system("yum reinstall -y " + name + " --downloadonly --downloaddir=./");

if __name__ == "__main__":
    lib_list = [];
    if(len(sys.argv) <= 1):
        print "Few args: yum_depsdown.py /lib-name/ ";
        exit();

    lib_name = sys.argv[1];

    fill_ignore_file_names();
    fill_ignore_hasdone_names();

    level = 0;
    deps_count = fill_depend_libs(lib_name, lib_list, level);

    print lib_name + " depend libs count=" + str(deps_count);
    
    ignore_file_depend_libs(lib_list);
    ignore_hasdone_libs(lib_list);

    index = 0;
    count = len(lib_list);

    fp = open(".deplibs", "w");
    for name in lib_list:
    	fp.write(name + "\n");
    fp.close();

    for name in lib_list:
        print "[%d/%d]:%s" % (index, count, name);
        index = index + 1;
        download_lib(name);
